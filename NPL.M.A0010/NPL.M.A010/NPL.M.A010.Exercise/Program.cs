﻿using NPL.M.A010.Exercise;
internal class Program
{

    public static void Menu()
    {
        Console.WriteLine("================Assignment 11 - Outlook Emulator =========");
        Console.WriteLine("Please Select Require: ");
        Console.WriteLine("1. New Mail");
        Console.WriteLine("2. Sent");
        Console.WriteLine("3. Draft");
        Console.WriteLine("4. Delete All Email");
        Console.WriteLine("5. Exit And Save");
        Console.Write("Enter Menu Option: ");
    }

    private static void Main(string[] args)
    {
        Manage.outLooks = Manage.ParseXmlToOutlookMailList("mail.xml");
        Manage manage = new Manage();
        int choice;
        do
        {
            Menu();
            choice = Convert.ToInt32(Console.ReadLine());
            switch (choice)
            {
                case 1: Manage.GetOutlookMailInput(); break;
                case 2:
                    
                    foreach (OutlookMail outLook in Manage.outLooks)
                    {
                        Console.WriteLine("\n======Email information======");
                        if (outLook.Status == MailStatus.Sent)
                            Console.WriteLine(outLook.ToString());
                    }
                    break;
                case 3:
                    foreach (OutlookMail outLook in Manage.outLooks)
                    {
                        if (outLook.Status == MailStatus.Draft)
                        {
                            Console.WriteLine(outLook.ToString());
                        }
                    }
                    break;
                case 4:
                    if (File.Exists("mail.xml"))
                    {
                        File.Delete("mail.xml");
                        Manage.outLooks.Clear();
                    }
                    break;
                case 5:
                    Manage.SaveXmlDocument(Manage.GenerateXmlDocuments(Manage.outLooks), "mail.xml");
                    return;
            }
        } while (true);
    }
}