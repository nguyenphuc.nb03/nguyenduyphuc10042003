﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise1
{
    internal class ArrayListExtension
    {
        
        public int CountInt(ArrayList array)
        {
            return array.OfType<int>().Count();
        }

        
        public int CountOf(ArrayList array, Type dataType)
        {
            return array.Cast<object>().Count(item => dataType.IsInstanceOfType(item));
        }

        
        public int CountOf<T>(ArrayList array)
        {
            return array.OfType<T>().Count();
        }

        public T MaxOf<T>(ArrayList array) where T : IComparable
        {
            var numericItems = array.OfType<T>();
            if (numericItems.Any())
            {
                return numericItems.Max();
            }
            throw new Exception("Không tìm thấy phần tử có kiểu số cụ thể trong ArrayList.");
        }
    }
}
