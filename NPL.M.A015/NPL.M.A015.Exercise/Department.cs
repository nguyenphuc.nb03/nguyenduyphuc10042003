﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A015.Exercise
{
    internal class Department
    {
        public int DeparmentId { get; set; }
        public string DepartmentName { get; set; }

        public Department() { }
        public Department(int deparmentId, string departmentName)
        {
            DeparmentId = deparmentId;
            DepartmentName = departmentName;
        }
    }
}
