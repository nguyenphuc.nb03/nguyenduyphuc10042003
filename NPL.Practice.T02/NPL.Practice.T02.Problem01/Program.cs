﻿using System;
using System.Text;

namespace NPL.Practice.T02.Problem01
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            Console.WriteLine("Nhập nội dung bài viết:");
            string content = Console.ReadLine();

            Console.WriteLine("Nhập chiều dài tối đa của tóm tắt:");
            int maxLength;
            if (int.TryParse(Console.ReadLine(), out maxLength))
            {
                string summary = GetArticleSummary(content, maxLength);
                Console.WriteLine("Tóm tắt: " + summary);
            }
            else
            {
                Console.WriteLine("Số không hợp lệ.");
            }
        }

        static public string GetArticleSummary(string content, int maxLength)
        {
            
            if (content.Length <= maxLength)
            {
                return content;
            }

            int cutIndex = maxLength - 3;
            while (cutIndex > 0 && !char.IsWhiteSpace(content[cutIndex]))
            {
                cutIndex--;
            }

            string summary = content.Substring(0, cutIndex) + "…";

            return summary;
        }
    }
}

