﻿using System;

namespace NPL.Practice.T02.Problem02
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Console.Write("Enter the elements of the array separated by spaces: ");
            int[] inputArray = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);

            Console.Write("Enter the subarray length: ");
            int subLength = int.Parse(Console.ReadLine());

            
            int maxSum = FindMaxSubArray(inputArray, subLength);
            Console.WriteLine("The maximum contiguous subarray sum is: " + maxSum);
        }

        static public int FindMaxSubArray(int[] inputArray, int subLength)
        {
            if (subLength > inputArray.Length)
            {
                throw new ArgumentException("Subarray length is greater than the array length.");
            }

            int maxSum = int.MinValue;
            int currentSum = 0;

            
            for (int i = 0; i < subLength; i++)
            {
                currentSum += inputArray[i];
            }

            maxSum = currentSum;

            
            for (int i = subLength; i < inputArray.Length; i++)
            {
                currentSum = currentSum - inputArray[i - subLength] + inputArray[i];
                maxSum = Math.Max(maxSum, currentSum);
            }

            return maxSum;
        }
    }
}
