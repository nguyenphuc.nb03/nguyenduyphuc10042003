﻿// See https://aka.ms/new-console-template for more information
using NPL.Practice.T02.Problem03;



internal class Program
{
    private static void Main(string[] args)
    {
        Student student = new Student
        {
            Id = 1,
            Name = "Peter",
            SqlMark = 10,
            CsharpMark = 9.5M,
            DsaMark = 10
        };


        student.Graduate();


        string certificate = student.GetCertificate();
        Console.WriteLine("Certificate: " + certificate);
}
}
