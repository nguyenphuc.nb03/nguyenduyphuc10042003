﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.Practice.T02.Problem03
{
    internal class Student: IGraduate
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public decimal SqlMark { get; set; }
        public decimal CsharpMark { get; set; }
        public decimal DsaMark { get; set; }
        public decimal GPA { get; set; }
        public GraduateLevel GraduateLevel { get; set; }

        public void Graduate()
        {
            
            GPA = (SqlMark + CsharpMark + DsaMark) / 3;

            
            if (GPA >= 9)
            {
                GraduateLevel = GraduateLevel.Excellent;
            }
            else if (GPA >= 8)
            {
                GraduateLevel = GraduateLevel.VeryGood;
            }
            else if (GPA >= 7)
            {
                GraduateLevel = GraduateLevel.Good;
            }
            else if (GPA >= 5)
            {
                GraduateLevel = GraduateLevel.AverageGood;
            }
            else
            {
                GraduateLevel = GraduateLevel.Failed;
            }
        }

        public string GetCertificate()
        {
            return $"Name: {Name}, SqlMark: {SqlMark}, CsharpMark: {CsharpMark}, DsaMark: {DsaMark}, GPA: {GPA:F2}, GraduateLevel: {GraduateLevel}.";
        }
    }
}

