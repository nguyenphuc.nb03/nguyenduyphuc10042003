﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A007.Exercise2.CarShop
{
    internal abstract class Car
    {
        public decimal Speed { get; set; }
        public double RegularPrice { get; set; }
        public string Color { get; set; }

        public Car() { }
        public Car(decimal Speed, double RegularPrice, string Color)
        {
            this.Speed = Speed;
            this.RegularPrice = RegularPrice;
            this.Color = Color;
        }
        public abstract double GetSalePrice();

    }


}
