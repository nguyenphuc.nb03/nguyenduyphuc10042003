﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A007.Exercise2.CarShop
{
    internal class Ford : Car
    {
        public int Year;
        public int ManufacturerDiscount;

        public Ford() { }
        public Ford(
            decimal Speed,
            double RegularPrice,
            string Color,
            int Year,
            int ManufacterDiscount) : base(Speed, RegularPrice, Color)
        {
            this.Year = Year;
            this.ManufacturerDiscount = ManufacterDiscount;
        }
        public override double GetSalePrice()
        {
            return this.RegularPrice - this.ManufacturerDiscount;
        }

        public void DisplaySalePrice()
        {
            Console.WriteLine("The sale prices of Ford Car :");
            string information = String.Format("{0, -10} {1, -20} {2, -12} {3, -13} {4, -30} {5,-13}\n",
                                               "Speed", "RegularPrice", "Color", "Year", "Manufacturer Discount", "Sales Price");
            information += String.Format("{0, -10} {1, -20} {2, -12} {3, -13} {4, -30} {5,-13}\n",
                                         Speed, RegularPrice, Color, Year, ManufacturerDiscount, GetSalePrice());
            Console.WriteLine(information);
        }

    }
}
